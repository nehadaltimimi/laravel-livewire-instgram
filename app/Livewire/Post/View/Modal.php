<?php

namespace App\Livewire\Post\View;

use App\Models\Post;
use Livewire\Component;
use LivewireUI\Modal\ModalComponent;

class Modal extends ModalComponent
{
    public $post;

    //  I copied this funtion from {ModalComponent} ..You can go to defintions and see alot of hlper functions
    public static function modalMaxWidth(): string
    {
        return  '5xl';
        // return config('wire-elements-modal.component_defaults.modal_max_width', '2xl');
    }


    //  I copied this funtion from {ModalComponent} ..You can go to defintions and see alot of hlper functions
    public static function closeModalOnEscape(): bool
    {
        return false;
        // return config('wire-elements-modal.component_defaults.close_modal_on_escape', true);
    }

    function mount()
    {
        $this->post = Post::findOrFail($this->post);

        #get url 
        $url = url('post/' . $this->post->id);
        #push state using new livewire v3 js helper ......
        #https://developer.mozilla.org/en-US/docs/Web/API/History/pushState
        $this->js("history.pushState({}, '', '{$url}')");
    }
    public function render()
    {
        return <<<'BLADE'
        <main  class="bg-white h-[calc(100vh_-_3.5rem)] md:h-[calc(100vh_-_5rem)] flex flex-col border gap-y-4 px-5">
            <header class="w-full py-2">
                <div class="flex justify-end">
                    <button wire:click="$dispatch('closeModal')"  type="button" class="xl font-bold">X</button>
                </div>
            </header>

            <livewire:post.view.item :post="$this->post" />
        </main>
        BLADE;
    }
}
