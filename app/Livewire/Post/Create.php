<?php

namespace App\Livewire\Post;

use App\Models\Post;
use App\Models\Media;
use LivewireUI\Modal\ModalComponent;
use Illuminate\Support\Facades\Storage;
use Livewire\Features\SupportFileUploads\WithFileUploads;


class Create extends ModalComponent
{
    use WithFileUploads;

    public $media = [];
    public $description;
    public $location;
    public $hide_like_view=false;
    public $allow_commenting=false;

    public function submit()
    {
        #Vildate
        $this->validate([
            'media.*' => 'required|file|mimes:png,jpg,mp4,jpeg,mov|max:50288',
            'hide_like_view'=> 'boolean',
            'allow_commenting'=> 'boolean',
        ]);
        #determine if real or post
        $type = $this->getPostType($this->media);

        #Create Post
        $post = Post::create([
            'user_id' => auth()->user()->id,
            'description' => $this->description,
            'location' => $this->location,
            'hide_like_view' => $this->hide_like_view,
            'allow_commenting' => $this->allow_commenting,
            'type' => $type
        ]);

        #add media
        foreach ($this->media as $key => $media) {
            #get mime type
            $mime = $this->getMime($media);

            #Save to storage
            $path = $media->store('media' , 'public');

            $url = url(Storage::url($path));

            #Create meida
            Media::create([
                'url' => $url,
                'mime' => $mime,
                'mediable_id' => $post->id,
                'mediable_type' => Post::class,
            ]);
            
        }
        $this->reset();
        $this->dispatch('close');

        #dispatch to listen livewire component Home 
        #reference livewire docs 
        $this->dispatch('post-created', $post->id);


        #add this 
        #In next video Get banner from pines-UI and add it to the layout
        return   $this->dispatch('created');

    }

   private function getMime($media):string{

        if (str()->contains($media->getMimeType(), 'video')) {
            return 'video';
        } else if (str()->contains($media->getMimeType(), 'image')) {
            return 'image';
        }
    }

   private function getPostType($media):string{
        // if (count($media) == 1 && str()->contains(needles: $media[0]->getMimeType(), ignoreCase: 'video')) {
        if (count($this->media) === 1 && str()->contains($this->media[0]->getMimeType(), 'video')) {
            return 'reel';
        }else {

            return 'post';
        }
    }

    /**
     * Supported: 'sm', 'md', 'lg', 'xl', '2xl', '3xl', '4xl', '5xl', '6xl', '7xl'
     */
    public static function modalMaxWidth(): string
    {
        return '4xl';
    }


    // public static function closeModalOnEscape(): bool
    // {
    //     return false;
    // }

    public function render()
    {
        return view('livewire.post.create');
    }
}
