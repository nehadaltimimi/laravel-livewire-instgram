import './bootstrap';
import { livewire_hot_reload } from 'virtual:livewire-hot-reload'
livewire_hot_reload();

import Alpine from 'alpinejs';

window.Alpine = Alpine;
import intersect from '@alpinejs/intersect'
Alpine.plugin(focus);

Alpine.plugin(intersect)
import Swiper from 'swiper';
import 'swiper/css';
import { Navigation, Pagination } from 'swiper/modules';

window.Alpine = Alpine;
window.Swiper = Swiper;
window.Navigation = Navigation;
window.Pagination = Pagination;

Alpine.start();

// // core version + navigation, pagination modules:
// import Swiper from 'swiper';
// import { Navigation, Pagination } from 'swiper/modules';
// // import 'swiper/swiper-bundle.css';
// // import Swiper and modules styles
// import 'swiper/css';
// // import 'swiper/css/navigation';
// import 'swiper/css/pagination';


// window.Swiper = Swiper;
// window.Navigation = Navigation;
// window.Pagination = Pagination;
