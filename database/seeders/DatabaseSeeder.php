<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Post;
use App\Models\Comment;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        // Post::factory(20)->create(['type' => 'reel']);
        // Post::factory(rand(10, 40))->create(['type' => 'post']);

        Post::factory(20)->hasComments(rand(20,20))->create(['type'=>'post']);
        Post::factory(12)->hasComments(rand(15,20))->create(['type'=>'reel']);

        // Create comment replies
        Comment::limit(50)->each(function($comment){
            $comment::factory(rand(1,5))->isReply($comment->commentable)->create(['parent_id'=>$comment->id]);

        });
    }
}
